var currentQuestion = 0;
var score = 0;

var questions = [
    {
        text: "Do you like warm weather?",
        answers: [
            { text: "Yes", correct: true },
            { text: "No", correct: false }
        ]
    },
    {
        text: "Do you like rain?",
        answers: [
            { text: "Yes", correct: true },
            { text: "No", correct: false }
        ]
    }
];

var questionContainer = document.getElementById("question-container");
var resultContainer = document.getElementById("result-container");
var questionTextElement = document.getElementById("question-text");
var answersListElement = document.getElementById("answers-list");
var resultText = document.getElementById("result-text");

function showQuestion() {
    var question = questions[currentQuestion];

    questionTextElement.textContent = question.text;

    // Clear previous answers
    while (answersListElement.firstChild) {
        answersListElement.removeChild(answersListElement.firstChild);
    }

    // Add new answers
    question.answers.forEach(function (answer, index) {
        var liElement = document.createElement("li");
        var button = document.createElement("button");
        button.textContent = answer.text;
        button.addEventListener("click", function () {
            checkAnswer(index);
        });
        liElement.appendChild(button);
        answersListElement.appendChild(liElement);
    });

    // Show the question container and hide the result container
    questionContainer.style.display = "block";
    resultContainer.style.display = "none";
}

function checkAnswer(answerIndex) {
    var question = questions[currentQuestion];
    var selectedAnswer = question.answers[answerIndex];

    if (selectedAnswer.correct) {
        score++;
        resultText.textContent = "Correct!";
    } else {
        resultText.textContent = "Incorrect!";
    }

    // Show the result container and hide the question container
    questionContainer.style.display = "none";
    resultContainer.style.display = "block";
}

function nextQuestion() {
    currentQuestion++;

    if (currentQuestion < questions.length) {
        showQuestion();
    } else {
        // All questions answered, show the final result
        questionContainer.style.display = "none";
        resultContainer.style.display = "block";
        resultText.textContent = "Quiz completed! Your score: " + score + " out of " + questions.length;
    }
}

// Start the quiz with the first question
showQuestion();
