from fastapi import FastAPI
from loguru import logger
from pydantic import BaseModel
from random import choice
from fastapi.middleware.cors import CORSMiddleware

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],  # Allows all origins for development
    allow_credentials=True,
    allow_methods=["*"],  # Allows all methods
    allow_headers=["*"],  # Allows all headers
)
questions = [
    {
        "text": "Do you like warm weather?",
        "answers": ["yes", "no", "maybe"]
    },
    {
        "text": "Do you like rain?",
        "answers": ["yes", "no"]
    }
    # Add more questions as needed
]


class Answer(BaseModel):
    username: str
    question: str
    answer: str


class UserScore:
    def __init__(self):
        self.score = 0


user_scores = {}


@app.get("/api/random-question")
def read_random_question():
    return choice(questions)


@app.post("/api/submit-answer")
def submit_answer(answer: Answer):
    if answer.username not in user_scores:
        user_scores[answer.username] = UserScore()

    if answer.answer == "yes":  # Assuming "yes" is the correct answer
        user_scores[answer.username].score += 1
    logger.info(f"User {answer.username} scored {user_scores[answer.username].score} points")
    return {"score": user_scores[answer.username].score}