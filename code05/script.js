let currentUsername = '';
let scoreDiv; // Declare scoreDiv globally

document.addEventListener("DOMContentLoaded", function() {
    const container = document.getElementById("quiz-container");
    scoreDiv = document.createElement("div"); // Initialize scoreDiv
    container.appendChild(scoreDiv);

    // Hide scoreDiv initially
    scoreDiv.style.display = "none";
});

function startQuiz() {
    currentUsername = document.getElementById('username').value;
    if (currentUsername) {
        document.getElementById('user-input').style.display = 'none';
        scoreDiv.style.display = "block"; // Show scoreDiv when quiz starts
        fetchNextQuestion();
    } else {
        alert('Please enter your name');
    }
}

function fetchNextQuestion() {
    fetch('http://127.0.0.1:8000/api/random-question')
        .then(response => response.json())
        .then(question => {
            displayQuestion(question);
        })
        .catch(error => console.error('Error fetching question:', error));
}

function displayQuestion(question) {
    const container = document.getElementById("quiz-container");
    container.innerHTML = ''; // Clear previous question
    container.appendChild(scoreDiv); // Re-add score display

    const questionDiv = document.createElement("div");
    questionDiv.classList.add("question");
    questionDiv.textContent = question.text;

    const answersDiv = document.createElement("div");
    answersDiv.classList.add("answers");

    // Dynamically create buttons for each answer
    question.answers.forEach(answer => {
        const button = document.createElement("button");
        button.textContent = answer;
        button.addEventListener("click", () => {
            submitAnswer(question.text, answer);
        });
        answersDiv.appendChild(button);
    });

    container.appendChild(questionDiv);
    container.appendChild(answersDiv);
}

function submitAnswer(questionText, answer) {
    fetch('http://127.0.0.1:8000/api/submit-answer', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({ username: currentUsername, question: questionText, answer: answer })
    })
    .then(response => response.json())
    .then(data => {
        scoreDiv.textContent = `Score for ${currentUsername}: ${data.score}`;
        fetchNextQuestion();
    })
    .catch(error => console.error('Error submitting answer:', error));
}
