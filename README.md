# how to build software with GPTs
Most of the Code is built using ChatGPT 3.5 and 4.0

Prompts used where:
```
User
Can you create a quiz game in html, css and javascript with the following details? 
Add two questions. Question one "Do you like warm weather?" with the answers "yes" and "no" and the second question: "Do you like rain?" with the answers "yes" and "no"
Store the questions in a json and generate the UI based on the questions in the JSON. Apply a pleasingly looking CSS which fits to a professional art exhibition 
```

To build a python backend edition this was used
```
Can you implement the same application but with an API request to a http backend which serves a random question
```

```
Can you generate the code for backend. Use python and FastApi 
```


## A Quiz Game App
code01 & code02 are two versions which were just formatted.

code03 and code04 not effectively doing much.

code05 is the first version that actually works with a python backend and basic user interface.

### Next Steps
* persist the scores in a database and show the highscore
* autogenerate questions
* add a timer
* introduce an AI player which replys to the questions